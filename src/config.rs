use std::{fs, io};

const CONFIG_FILE_PATH: &str = "/etc/spline/splined.conf";

#[derive(Deserialize)]
pub struct Tokens {
    pub telegram: Option<String>,
    pub mattermost: Option<String>,
    pub spaceapi: Option<String>,
    pub spaceapi_endpoint: Option<String>,
    pub spaceapi_space_id: Option<String>,
}

#[derive(Deserialize)]
pub struct Gpio {
    pub door_pin: u32,
}

#[derive(Deserialize)]
pub struct Config {
    pub tokens: Tokens,
    pub gpio: Gpio,
}

impl Config {
    pub fn open() -> io::Result<Config> {
        let config_str = fs::read_to_string(CONFIG_FILE_PATH)?;
        match toml::from_str::<Config>(&config_str) {
            Err(e) => Err(io::Error::new(io::ErrorKind::InvalidData, e.message())),
            Ok(config_str) => Ok(config_str),
        }
    }
}
