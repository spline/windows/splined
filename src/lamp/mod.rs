use std::process::Command;
use std::thread;
use std::time::Duration;

const MONITORING_URL: &str =
    "https://monitoring.spline.inf.fu-berlin.de/view/icinga/cgi-bin/tac.cgi?jsonoutput";

pub enum LampStatus {
    Green,
    Red,
    Yellow,
}

impl LampStatus {
    fn to_string(&self) -> &'static str {
        use LampStatus::*;
        match self {
            Green => "green",
            Red => "red",
            Yellow => "yellow",
        }
    }
}

fn detect_services_status() -> Result<LampStatus, ureq::Error> {
    let response = ureq::get(MONITORING_URL).call()?;
    let json = response.into_json::<ureq::serde_json::value::Value>()?;
    let data = &json["tac"]["tac_overview"];

    if (data["services_warning"].as_i64().unwrap_or(0)
        - data["services_warning_scheduled"].as_i64().unwrap_or(0))
        > 0
    {
        return Ok(LampStatus::Yellow);
    }

    if (data["services_critical"].as_i64().unwrap_or(0)
        - data["services_critical_scheduled"].as_i64().unwrap_or(0))
        > 0
    {
        return Ok(LampStatus::Red);
    }

    Ok(LampStatus::Green)
}

fn set_lamp(status: LampStatus) {
    match Command::new("usblamp").arg(status.to_string()).status() {
        Ok(status) => {
            if !status.success() {
                eprintln!("Failed to set lamp status");
            }
        }
        Err(e) => {
            eprintln!("{:?}", e);
        }
    }
}

fn fetch_and_set_lamp() {
    match detect_services_status() {
        Ok(status) => set_lamp(status),
        Err(e) => {
            eprintln!("{:?}", e);
        }
    }
}

pub fn watch_monitoring() {
    thread::spawn(|| loop {
        fetch_and_set_lamp();
        thread::sleep(Duration::from_secs(10));
    });
}
