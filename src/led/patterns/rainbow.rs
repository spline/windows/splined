use super::super::*;

use std::time::Duration;

pub struct Rainbow {}

impl Rainbow {
    pub fn new() -> Rainbow {
        Rainbow {}
    }
}

pub fn wheel(mut pos: u8) -> Rgb {
    if pos < 85 {
        return (pos * 3, 255 - pos * 3, 0);
    }
    if pos < 170 {
        return (255 - pos * 3, 0, pos * 3);
    }
    pos -= 170;

    (0, pos * 3, 255 - pos * 3)
}

impl LightPattern for Rainbow {
    fn start(
        &self,
        mut adapter_sender: AdapterSender,
        cancel: &mut mpsc::Receiver<()>,
    ) -> Result<(), StopReason> {
        let wait_ms = Duration::from_millis(1000);

        let mut data = Vec::<Rgb>::new();

        for j in 0..256 {
            for (i, item) in data.iter_mut().enumerate().take(constants::NUM_LEDS) {
                let pos = (i + j) & 255;
                *item = wheel(pos as u8);
            }

            submit_and_maybe_yield(data.clone(), &mut adapter_sender, cancel)?;
            std::thread::sleep(wait_ms);
        }

        Ok(())
    }
}
