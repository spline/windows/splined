use super::super::*;

use crate::led::constants;

use std::time::Duration;

pub struct ColorWiper {
    color: Rgb,
}

impl ColorWiper {
    pub fn new(color: Rgb) -> ColorWiper {
        ColorWiper { color }
    }
}

impl LightPattern for ColorWiper {
    fn start(
        &self,
        mut adapter_sender: AdapterSender,
        cancel: &mut mpsc::Receiver<()>,
    ) -> Result<(), StopReason> {
        // TODO: Choose color, speed etc. in constructor
        let wait_ms = Duration::from_millis(50);

        let mut data: Vec<Rgb> = Vec::new();
        data.resize(constants::NUM_LEDS, (0, 0, 0));
        for i in 0..constants::NUM_LEDS {
            data[i] = self.color;
            submit_and_maybe_yield(data.clone(), &mut adapter_sender, cancel)?;

            std::thread::sleep(wait_ms);
        }

        Ok(())
    }
}
