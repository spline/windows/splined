use super::super::*;

use std::time::Duration;

use super::rainbow::wheel;

pub struct RainbowCycle {}

impl RainbowCycle {
    pub fn new() -> RainbowCycle {
        RainbowCycle {}
    }
}

impl LightPattern for RainbowCycle {
    fn start(
        &self,
        mut adapter_sender: AdapterSender,
        cancel: &mut mpsc::Receiver<()>,
    ) -> Result<(), StopReason> {
        let mut data = Vec::<Rgb>::new();
        data.resize(constants::NUM_LEDS, (0, 0, 0));

        for j in 0..256 {
            for (i, item) in data.iter_mut().enumerate().take(constants::NUM_LEDS) {
                *item = wheel((((i * 256 / constants::NUM_LEDS) + j) & 255) as u8);
            }

            submit_and_maybe_yield(data.clone(), &mut adapter_sender, cancel)?;
            std::thread::sleep(Duration::from_millis(1000));
        }

        Ok(())
    }
}
