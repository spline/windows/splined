use super::super::*;

use super::rainbow::wheel;

use std::thread;
use std::time::Duration;

pub struct TheaterChaseRainbow {}

impl TheaterChaseRainbow {
    pub fn new() -> TheaterChaseRainbow {
        TheaterChaseRainbow {}
    }
}

impl LightPattern for TheaterChaseRainbow {
    fn start(
        &self,
        mut adapter_sender: AdapterSender,
        cancel: &mut mpsc::Receiver<()>,
    ) -> Result<(), StopReason> {
        for j in 0..156 {
            let mut data = Vec::<Rgb>::new();
            data.resize(constants::NUM_LEDS, (0, 0, 0));
            for q in 0..3 {
                for i in 0..constants::NUM_LEDS {
                    if (i + q) < constants::NUM_LEDS {
                        data[i + q] = wheel(((i + j) % 255) as u8);
                    }
                }

                submit_and_maybe_yield(data.clone(), &mut adapter_sender, cancel)?;
                thread::sleep(Duration::from_millis(1000));

                for i in (0..constants::NUM_LEDS).step_by(3) {
                    if (i + q) < constants::NUM_LEDS {
                        data[i + q] = (0, 0, 0)
                    }
                }
            }
        }
        Ok(())
    }
}
