use std::thread;
use std::time::Duration;

use super::super::*;

use crate::led::constants;

pub struct ColorFill {
    color: Rgb,
}

impl ColorFill {
    pub fn new(color: Rgb) -> ColorFill {
        ColorFill { color }
    }
}

impl LightPattern for ColorFill {
    fn start(
        &self,
        mut adapter_sender: AdapterSender,
        cancel: &mut mpsc::Receiver<()>,
    ) -> Result<(), StopReason> {
        // The hardware SPI is subtly broken. Re-paint the pattern frequently enough to counteract the drift.
        loop {
            let mut data: Vec<Rgb> = Vec::new();
            for _ in 0..constants::NUM_LEDS {
                data.push(self.color);
            }
            submit_and_maybe_yield(data, &mut adapter_sender, cancel)?;
            thread::sleep(Duration::from_secs(1));
        }
    }
}
