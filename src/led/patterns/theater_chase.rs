use crate::led::*;

use std::sync::mpsc;

use std::time::Duration;

pub struct TheaterChase {
    color: Rgb,
}

impl TheaterChase {
    pub fn new(color: Rgb) -> TheaterChase {
        TheaterChase { color }
    }
}

impl LightPattern for TheaterChase {
    fn start(
        &self,
        mut adapter_sender: AdapterSender,
        cancel: &mut mpsc::Receiver<()>,
    ) -> Result<(), StopReason> {
        let iterations = 10;
        let wait_ms = Duration::from_millis(50);

        for _ in 0..iterations {
            for q in 0..3 {
                let mut data: Vec<Rgb> = Vec::new();
                data.resize(constants::NUM_LEDS, (0, 0, 0));
                for i in (0..constants::NUM_LEDS).step_by(3) {
                    if (i + q) < constants::NUM_LEDS {
                        data[i + q] = self.color;
                    }
                }
                submit_and_maybe_yield(data, &mut adapter_sender, cancel)?;

                std::thread::sleep(wait_ms);

                let mut data: Vec<Rgb> = Vec::new();
                data.resize(constants::NUM_LEDS, (0, 0, 0));
                for i in (0..constants::NUM_LEDS).step_by(3) {
                    if (i + q) < constants::NUM_LEDS {
                        data[i + q] = self.color;
                    }
                }
            }
        }

        Ok(())
    }
}
