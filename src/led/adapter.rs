use std::sync::mpsc;
use std::sync::mpsc::SyncSender;
use std::thread;

use ws2818_rgb_led_spi_driver::adapter_gen::WS28xxAdapter;
use ws2818_rgb_led_spi_driver::adapter_spi::WS28xxSpiAdapter;

/// Thread that owns the LED adapter and talks to it.
/// All other threads must send messages to this thread in order to display using the LEDs.
///
/// This avoids some tricky ownership issues and requiring the adapter to be Send.
pub fn create_adapter_thread() -> (mpsc::SyncSender<Vec<super::Rgb>>, thread::JoinHandle<()>) {
    let (tx, rx): (SyncSender<Vec<super::Rgb>>, _) = mpsc::sync_channel(16384);

    let handle = thread::spawn(move || {
        let mut adapter = WS28xxSpiAdapter::new("/dev/spidev0.0").unwrap();
        // Wait on messages and process them by dumping into the adapter
        for msg in rx {
            adapter.write_rgb(&msg).unwrap();
        }
    });

    (tx, handle)
}
