//! This module allows remote controlling the LED pattern over TCP.
//! See https://doku.spline.de/splineschild?s[]=windows for info about the protocol.

use std::io;
use std::io::{BufRead, BufReader};
use std::net::{TcpListener, TcpStream};
use std::sync::mpsc;
use std::thread;

use super::*;

const PORT: u32 = 8443;
const BIND_ADDR: &str = "0.0.0.0";

/// Possible commands for the server.
#[derive(Copy, Clone, Debug)]
enum Cmd {
    Color(Rgb),
    Rainbow,
    RainbowCycle,
    ColorWipeRed,
    ColorWipeGreen,
    ColorWipeBlue,
    TheaterRainbowCycle,
    TheaterChaseRed,
    TheaterChaseGreen,
    TheaterChaseBlue,
}

impl Cmd {
    fn start_displaying(&self, comm: AdapterSender) {
        cancel_current_pattern();

        let (cancel_tx, cancel_rx) = mpsc::channel();
        let (confirm_tx, confirm_rx) = mpsc::channel();
        *crate::CANCEL_PATTERN_SENDER.lock().unwrap() = Some(crate::Cancel {
            cancel: cancel_tx,
            cancel_confirmed: confirm_rx,
        });

        use Cmd::*;
        match self {
            Color(rgb) => start_pattern(
                comm,
                Box::new(patterns::ColorFill::new(*rgb)),
                cancel_rx,
                confirm_tx,
            ),
            TheaterChaseRed => start_pattern(
                comm,
                Box::new(patterns::TheaterChase::new((255, 0, 0))),
                cancel_rx,
                confirm_tx,
            ),
            TheaterChaseGreen => start_pattern(
                comm,
                Box::new(patterns::TheaterChase::new((0, 255, 0))),
                cancel_rx,
                confirm_tx,
            ),
            TheaterChaseBlue => start_pattern(
                comm,
                Box::new(patterns::TheaterChase::new((0, 0, 255))),
                cancel_rx,
                confirm_tx,
            ),
            ColorWipeRed => start_pattern(
                comm,
                Box::new(patterns::ColorWiper::new((255, 0, 0))),
                cancel_rx,
                confirm_tx,
            ),
            ColorWipeGreen => start_pattern(
                comm,
                Box::new(patterns::ColorWiper::new((0, 255, 0))),
                cancel_rx,
                confirm_tx,
            ),
            ColorWipeBlue => start_pattern(
                comm,
                Box::new(patterns::ColorWiper::new((0, 0, 255))),
                cancel_rx,
                confirm_tx,
            ),
            Rainbow => start_pattern(
                comm,
                Box::new(patterns::Rainbow::new()),
                cancel_rx,
                confirm_tx,
            ),
            RainbowCycle => {
                start_pattern(
                    comm,
                    Box::new(patterns::RainbowCycle::new()),
                    cancel_rx,
                    confirm_tx,
                );
            }
            TheaterRainbowCycle => {
                start_pattern(
                    comm,
                    Box::new(patterns::TheaterChaseRainbow::new()),
                    cancel_rx,
                    confirm_tx,
                );
            }
        }
    }
}

/// Possible errors encountered when handling a client.
#[derive(Debug)]
enum Err {
    IoError(io::Error),
    InvalidRGBValue(String),
    UnknownCommand(String),
}

impl From<io::Error> for Err {
    fn from(e: io::Error) -> Self {
        Err::IoError(e)
    }
}

/// Starts a server thread, then returns.
pub fn start_server(led_communicator: AdapterSender) {
    thread::spawn(move || {
        let result = TcpListener::bind(format!("{}:{}", BIND_ADDR, PORT));
        let listener = result.expect("Failed to start listener for LED remote control protocl");

        for incoming in listener.incoming() {
            let comm = led_communicator.clone();
            match incoming {
                Ok(mut stream) => {
                    thread::spawn(move || match handle(&mut stream, comm) {
                        Ok(_) => (),
                        Err(e) => {
                            eprintln!("Failed to handle client {:?}: {:?}", stream.peer_addr(), e)
                        }
                    });
                }
                Err(e) => eprintln!("Client failed to connect: {}", e),
            }
        }
    });
}

fn handle(s: &mut TcpStream, comm: AdapterSender) -> Result<(), Err> {
    // Read until the connection is closed.
    let mut buf = String::new();
    let mut reader = BufReader::new(s);
    while let Ok(size) = reader.read_line(&mut buf) {
        eprintln!("Received {}", size);
        if size > 0 {
            let cmd = data_to_cmd(buf.trim())?;
            cmd.start_displaying(comm.clone());
            buf.clear();
        } else {
            return Ok(());
        }
    }
    // TODO: Send cancellation to previous cmd, if any
    Ok(())
}

fn data_to_cmd(data: &str) -> Result<Cmd, Err> {
    // First character is a digit => try to parse as RGB color
    if data.chars().next().unwrap().is_ascii_digit() {
        let rgbs: Vec<&str> = data.split(' ').collect();
        let rgbs: Vec<u8> = rgbs.iter().map(|x| x.parse::<u8>().unwrap()).collect();
        // Must be exactly 3 numbers
        if rgbs.len() != 3 {
            return Err(Err::InvalidRGBValue(String::from("Not 3 numbers")));
        }
        return Ok(Cmd::Color((rgbs[0], rgbs[1], rgbs[2])));
    }

    // All others are static strings
    match data {
        "rb" => Ok(Cmd::Rainbow),
        "rbc" => Ok(Cmd::RainbowCycle),
        "cwr" => Ok(Cmd::ColorWipeRed),
        "cwg" => Ok(Cmd::ColorWipeGreen),
        "cwb" => Ok(Cmd::ColorWipeBlue),
        "trc" => Ok(Cmd::TheaterRainbowCycle),
        "tcr" => Ok(Cmd::TheaterChaseRed),
        "tcg" => Ok(Cmd::TheaterChaseGreen),
        "tcb" => Ok(Cmd::TheaterChaseBlue),
        _ => Err(Err::UnknownCommand(String::from(data))),
    }
}
