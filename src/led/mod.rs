use std::sync::mpsc;
use std::sync::mpsc::SyncSender;

pub mod adapter;
pub use adapter::create_adapter_thread;

pub mod constants;
pub mod patterns;
pub mod tcp;

type Rgb = (u8, u8, u8);
type AdapterSender = SyncSender<Vec<Rgb>>;
pub trait LightPattern: Sync + Send {
    fn start(
        &self,
        adapter_sender: AdapterSender,
        cancel: &mut mpsc::Receiver<()>,
    ) -> Result<(), StopReason>;
}

#[derive(Debug)]
pub enum StopReason {
    StoppedByUser,
}

// TODO: Error handling
fn submit_and_maybe_yield(
    data: Vec<Rgb>,
    adapter_sender: &mut AdapterSender,
    cancel: &mut mpsc::Receiver<()>,
) -> Result<(), StopReason> {
    // Submit data to adapter
    // TODO: Proper error handling/retry
    adapter_sender.send(data).unwrap();

    // Decide whether to yield
    if cancel.try_recv().is_ok() {
        // Simply yield by terminating our thread
        return Err(StopReason::StoppedByUser);
    }

    Ok(())
}

pub fn start_pattern(
    adapter_sender: AdapterSender,
    p: Box<dyn LightPattern>,
    mut cancel: mpsc::Receiver<()>,
    cancel_confirm: mpsc::Sender<()>,
) {
    std::thread::spawn(move || {
        if let Err(stop_reason) = p.start(adapter_sender, &mut cancel) {
            eprintln!("Pattern stopped before it could finish: {:?}", stop_reason);
        }
        eprintln!("Confirming cancellation");
        cancel_confirm
            .send(())
            .expect("Failed to confirm cancellation");
    });
}

pub fn cancel_current_pattern() {
    if let Some(ref mut state) = *crate::CANCEL_PATTERN_SENDER.lock().unwrap() {
        if let Err(e) = state.cancel.send(()) {
            eprintln!("Failed to cancel running pattern: {e}");
        } else {
            eprintln!("Cancelled pattern");
            state.cancel_confirmed.recv().expect("Channel hung up");
            eprintln!("Received confirmation");
        }
    } else {
        eprintln!("There was nothing to cancel");
    }
    // Maybe not the most correct thing to assume success, but it works
    *crate::CANCEL_PATTERN_SENDER.lock().unwrap() = None;
}
