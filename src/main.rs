#![forbid(unsafe_code)]

use lazy_static::lazy_static;
use std::panic;
use std::process;
use std::sync::mpsc;
use std::sync::Arc;
use std::sync::Mutex;

use cfg_if::cfg_if;

mod door;
#[cfg(feature = "spline-room-integration")]
mod lamp;
#[cfg(feature = "spline-room-integration")]
mod led;

struct Cancel {
    cancel: mpsc::Sender<()>,
    cancel_confirmed: mpsc::Receiver<()>,
}

lazy_static! {
    pub(crate) static ref CANCEL_PATTERN_SENDER: Mutex<Option<Cancel>> = Mutex::from(None);
}

mod config;

use crate::config::Config;

#[macro_use]
extern crate serde;

fn main() {
    let config = Arc::from(Config::open().expect("Failed to read config file"));

    // Install handler to panic main thread when any child thread panics.
    // This way, systemd knows when we crashed and to restart us.
    let orig_hook = panic::take_hook();
    panic::set_hook(Box::new(move |panic_info| {
        orig_hook(panic_info);
        process::exit(1);
    }));

    cfg_if! {
        if #[cfg(feature = "spline-room-integration")] {
            let (adapter_sender, _) = led::create_adapter_thread();
            door::watcher::start(config, adapter_sender.clone());
            led::tcp::start_server(adapter_sender);
            lamp::watch_monitoring();
        } else {
            let (tx, rx) = mpsc::sync_channel(0);
            door::watcher::start(config, tx);
        }
    }

    // Main thread has to remain alive
    loop {
        std::thread::sleep(std::time::Duration::from_secs(1));
    }
}
