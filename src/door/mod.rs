use gpio_cdev::{Chip, LineRequestFlags};

#[cfg(feature = "spline-room-integration")]
mod mattermost;
#[cfg(feature = "spline-room-integration")]
mod telegram;

mod spaceapi;

#[cfg(feature = "spline-room-integration")]
mod tick;
pub mod watcher;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum DoorState {
    Open,
    Closed,
}

impl DoorState {
    pub fn get(pin: u32) -> Result<DoorState, gpio_cdev::Error> {
        let mut chip = Chip::new("/dev/gpiochip0")?;
        let input = chip.get_line(pin)?;
        let line = input.request(LineRequestFlags::INPUT, 0, "splined")?;

        match line.get_value()? {
            0 => Ok(DoorState::Closed),
            1 => Ok(DoorState::Open),
            _ => panic!("Unexpected value received from GPIO"),
        }
    }
}
