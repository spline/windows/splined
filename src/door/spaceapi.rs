use std::time::Duration;

const TIMEOUT: Duration = Duration::from_secs(10);

pub fn report_door(
    endpoint: &str,
    token: &str,
    space: &str,
    open: bool,
) -> Result<(), ureq::Error> {
    ureq::post(&format!(
        "{endpoint}/{space}/door/{}",
        if open { "open" } else { "close" }
    ))
    .set("spaceapi-token", token)
    .timeout(TIMEOUT)
    .call()?;
    Ok(())
}

pub fn report_open(endpoint: &str, token: &str, space: &str) -> Result<(), ureq::Error> {
    report_door(endpoint, token, space, true)
}

pub fn report_closed(endpoint: &str, token: &str, space: &str) -> Result<(), ureq::Error> {
    report_door(endpoint, token, space, false)
}
