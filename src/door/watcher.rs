use std::fmt;
use std::{sync::mpsc, sync::mpsc::SyncSender, sync::Arc, thread, time};

use cfg_if::cfg_if;

#[cfg(feature = "spline-room-integration")]
use crate::led;

#[cfg(feature = "spline-room-integration")]
use super::mattermost;
use super::spaceapi;
#[cfg(feature = "spline-room-integration")]
use super::telegram;
#[cfg(feature = "spline-room-integration")]
use super::tick;
use super::DoorState;

use crate::config::Config;

type DoorResult<T> = Result<T, DoorError>;

const DEFAULT_SPACEAPI_ENDPOINT_URL: &str = "https://spaceapi.spline.de/";
const DEFAULT_SPACEAPI_SPACE_ID: &str = "spline";

fn print_error(result: DoorResult<()>) {
    if let Err(error) = result {
        eprintln!("door: Failed to report door status: {:?}", error);
    }
}

pub fn start(config: Arc<Config>, led_communicator: SyncSender<Vec<(u8, u8, u8)>>) {
    thread::spawn(move || {
        // Initialize Telegram bot
        #[cfg(feature = "spline-room-integration")]
        let mut tg = config
            .tokens
            .telegram
            .as_ref()
            .map(|t| telegram::DoorNotifier::new(t));
        let mut old_state: Option<DoorState> = None;
        loop {
            // Only trigger changes if different from last door state
            let new_state = DoorState::get(config.gpio.door_pin).unwrap();

            if let Some(old) = old_state {
                if old != new_state {
                    eprintln!(
                        "door: New door state: {:?}, old door state: {:?}",
                        new_state, old_state
                    );
                } else {
                    //eprintln!("Door state stayed the same");
                    thread::sleep(time::Duration::from_secs(1));
                    continue;
                }
            } else {
                eprintln!("Door state unknown");
            }

            cfg_if! {
                if #[cfg(feature = "spline-room-integration")] {
                    let (cancel_tx, cancel_rx) = mpsc::channel();
                    let (confirm_tx, confirm_rx) = mpsc::channel();
                    led::cancel_current_pattern();
                    *crate::CANCEL_PATTERN_SENDER.lock().unwrap() = Some(crate::Cancel {
                        cancel: cancel_tx,
                        cancel_confirmed: confirm_rx,
                    });
                } else {
                    let (cancel_tx, cancel_rx) = mpsc::channel();
                    let (confirm_tx, confirm_rx) = mpsc::channel();
                }
            }

            // Is state the same?
            use DoorState::*;
            match old_state {
                Some(Open) => match new_state {
                    Open => (),
                    Closed => print_error(report_door_closed(
                        config.clone(),
                        led_communicator.clone(),
                        cancel_rx,
                        confirm_tx,
                        #[cfg(feature = "spline-room-integration")]
                        tg.as_mut(),
                    )),
                },
                Some(Closed) => match new_state {
                    Closed => (),
                    Open => print_error(report_door_open(
                        config.clone(),
                        led_communicator.clone(),
                        cancel_rx,
                        confirm_tx,
                        #[cfg(feature = "spline-room-integration")]
                        tg.as_mut(),
                    )),
                },
                // If we have no data about the previous state, execute all actions anyways to
                // ensure correct state. Downside of this is that it may lead to duplicate
                // notifications on restart.
                None => match new_state {
                    Open => print_error(report_door_open(
                        config.clone(),
                        led_communicator.clone(),
                        cancel_rx,
                        confirm_tx,
                        #[cfg(feature = "spline-room-integration")]
                        tg.as_mut(),
                    )),
                    Closed => print_error(report_door_closed(
                        config.clone(),
                        led_communicator.clone(),
                        cancel_rx,
                        confirm_tx,
                        #[cfg(feature = "spline-room-integration")]
                        tg.as_mut(),
                    )),
                },
            }

            // For now, check whether door is open once a second
            thread::sleep(time::Duration::from_secs(1));
            old_state = Some(new_state);
        }
    });
}

#[derive(Debug)]
enum DoorError {
    Http(ureq::Error),
    #[cfg(feature = "spline-room-integration")]
    Tick(tick::TickError),
    #[cfg(feature = "spline-room-integration")]
    Telegram(telbot_ureq::Error),
}

impl fmt::Display for DoorError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use DoorError::*;
        match self {
            Http(e) => e.fmt(f),
            #[cfg(feature = "spline-room-integration")]
            Tick(e) => e.fmt(f),
            #[cfg(feature = "spline-room-integration")]
            Telegram(e) => {
                use telbot_ureq::Error::*;
                match e {
                    TelegramError(e) => write!(f, "{}", e.description),
                    Ureq(e) => e.fmt(f),
                    Serde(e) => e.fmt(f),
                    Io(e) => e.fmt(f),
                }
            }
        }
    }
}

impl From<ureq::Error> for DoorError {
    fn from(error: ureq::Error) -> Self {
        DoorError::Http(error)
    }
}

#[cfg(feature = "spline-room-integration")]
impl From<tick::TickError> for DoorError {
    fn from(error: tick::TickError) -> Self {
        DoorError::Tick(error)
    }
}

#[cfg(feature = "spline-room-integration")]
impl From<telbot_ureq::Error> for DoorError {
    fn from(error: telbot_ureq::Error) -> Self {
        DoorError::Telegram(error)
    }
}

fn report_door_open(
    config: Arc<Config>,
    led_communicator: SyncSender<Vec<(u8, u8, u8)>>,
    cancel: mpsc::Receiver<()>,
    confirm_tx: mpsc::Sender<()>,
    #[cfg(feature = "spline-room-integration")]
    tg: Option<&mut telegram::DoorNotifier>,
) -> DoorResult<()> {
    eprintln!("door: Reporting that door has been opened");
    cfg_if! {
        if #[cfg(feature = "spline-room-integration")] {
            let pattern = led::patterns::ColorFill::new((0, 255, 0));
            led::start_pattern(led_communicator, Box::new(pattern), cancel, confirm_tx);

            if let Some(ref mattermost) = config.tokens.mattermost {
                mattermost::report_open(mattermost)?;
            }

            tick::report_open()?;
            if let Some(tg) = tg {
                tg.report_open()?;
            }
        }
    }

    if let Some(ref token) = config.tokens.spaceapi {
        spaceapi::report_open(
            &config
                .tokens
                .spaceapi_endpoint
                .clone()
                .unwrap_or_else(|| DEFAULT_SPACEAPI_ENDPOINT_URL.to_string()),
            token,
            &config
                .as_ref()
                .tokens
                .spaceapi_space_id
                .clone()
                .unwrap_or_else(|| DEFAULT_SPACEAPI_SPACE_ID.to_string()),
        )?;
    }

    Ok(())
}

fn report_door_closed(
    config: Arc<Config>,
    led_communicator: SyncSender<Vec<(u8, u8, u8)>>,
    cancel: mpsc::Receiver<()>,
    confirm_tx: mpsc::Sender<()>,
    #[cfg(feature = "spline-room-integration")]
    tg: Option<&mut telegram::DoorNotifier>,
) -> DoorResult<()> {
    eprintln!("door: Reporting that door has been closed");
    cfg_if! {
        if #[cfg(feature = "spline-room-integration")] {
            let pattern = led::patterns::ColorFill::new((255, 0, 0));
            led::start_pattern(led_communicator, Box::new(pattern), cancel, confirm_tx);

            if let Err(e) = tick::report_closed() {
                eprintln!("dock: Failed to shut down tick {}", e)
            }

            if let Some(ref mattermost) = config.tokens.mattermost {
                if let Err(e) = mattermost::report_closed(mattermost) {
                    eprintln!("door: Failed to report to mattermost {}", e);
                }
            }

            if let Some(tg) = tg {
                if let Err(e) = tg.report_closed() {
                    eprintln!("door: Failed to report to telegram {:?}", e);
                }
            }
        }
    }

    if let Some(ref token) = config.tokens.spaceapi {
        let result = spaceapi::report_closed(
            &config
                .tokens
                .spaceapi_endpoint
                .clone()
                .unwrap_or_else(|| DEFAULT_SPACEAPI_ENDPOINT_URL.to_string()),
            token,
            &config
                .as_ref()
                .tokens
                .spaceapi_space_id
                .clone()
                .unwrap_or_else(|| DEFAULT_SPACEAPI_SPACE_ID.to_string()),
        );
        if let Err(e) = result {
            eprintln!("door: Failed to report to SpaceAPI: {}", e);
        }
    }

    Ok(())
}
