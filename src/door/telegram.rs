use std::fs::File;
use std::io;
use std::io::ErrorKind as IOErrorKind;
use std::io::Read;
use std::os::unix::fs::FileExt;
use std::path::PathBuf;
use std::time::Duration;

use telbot_ureq::types::chat::ChatId;
use telbot_ureq::types::file::InputFile;
use telbot_ureq::types::message::{DeleteMessage, SendMessage, SendVideo};
use telbot_ureq::Api;

const CHAT_ID: ChatId = ChatId::Id(-1001352936285);
const DOOR_OPEN_MSG: &str = "The fucking Door is open";
const DOOR_CLOSED_GIF: &[u8] = include_bytes!("../../gendalf.mp4");
const DOOR_CLOSED_GIF_NAME: &str = "gendalf.gif";
const DOOR_CLOSED_CAPTION: &str = "The door is closed!";
const TIMEOUT: Duration = Duration::from_secs(10);

pub struct DoorNotifier {
    api: Api,
}

fn print_file_error(result: io::Result<()>) {
    if let Err(error) = result {
        eprintln!(
            "telegram: Failed to access telegram last message id file: {}",
            error
        );
    }
}

fn get_tg_cache_file() -> PathBuf {
    let xdg_dirs = xdg::BaseDirectories::with_prefix("splined").unwrap();
    xdg_dirs.place_cache_file("last_telegram_msg").unwrap()
}

impl DoorNotifier {
    fn set_last_message_id(&mut self, id: i64) -> io::Result<()> {
        eprintln!("telegram: Setting last message to {}", id);
        let file = File::create(get_tg_cache_file())?;
        file.write_at(&i64::to_be_bytes(id), 0)?;
        Ok(())
    }

    fn get_last_message_id(&mut self) -> io::Result<i64> {
        let mut content: [u8; 8] = Default::default();
        let mut file = File::open(get_tg_cache_file())?;
        file.read_exact(&mut content)?;
        let id = i64::from_be_bytes(content);
        // If file was empty, this reads 0. Abort.
        if id == 0 {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                "Read 0 as last Telegram message ID. This probably means that the file was empty.",
            ));
        }
        eprintln!("telegram: Last message was {}", id);
        Ok(id)
    }

    pub fn new(token: &str) -> DoorNotifier {
        let mut api = Api::new(token);
        api.set_timeout(TIMEOUT);

        if let Err(e) = File::open(get_tg_cache_file()) {
            if e.kind() == IOErrorKind::NotFound {
                File::create(get_tg_cache_file()).expect("Failed to create watcher cache file");
            } else {
                panic!("{:?}", e)
            }
        }

        DoorNotifier { api }
    }

    pub fn report_open(&mut self) -> telbot_ureq::Result<()> {
        if let Err(e) = self.delete_last_msg() {
            eprintln!("telegram: Failed to delete last message {:?}", e);
        }
        let msg = SendMessage::new(CHAT_ID, DOOR_OPEN_MSG);
        let resp = self.api.send_json(&msg)?;
        print_file_error(self.set_last_message_id(resp.message_id));
        Ok(())
    }

    pub fn report_closed(&mut self) -> telbot_ureq::Result<()> {
        if let Err(e) = self.delete_last_msg() {
            eprintln!("telegram: Failed to delete last message {:?}", e);
        }

        // Send actual chat message
        let photo = InputFile {
            name: DOOR_CLOSED_GIF_NAME.to_string(),
            data: Vec::from(DOOR_CLOSED_GIF),
            mime: "video/mp4".to_string(),
        };
        let msg = SendVideo::new(CHAT_ID, photo).with_caption(DOOR_CLOSED_CAPTION);
        let resp = self.api.send_file(&msg)?;
        print_file_error(self.set_last_message_id(resp.message_id));
        Ok(())
    }

    fn delete_last_msg(&mut self) -> telbot_ureq::Result<()> {
        let id = self.get_last_message_id();
        match id {
            Ok(id_inner) => {
                let del = DeleteMessage::new(CHAT_ID, id_inner);
                self.api.send_json(&del)?;
            }
            Err(e) => {
                eprintln!(
                    "telegram: Failed to delete old message, ignoring. Error: {:?}",
                    e
                )
            }
        }

        Ok(())
    }
}
