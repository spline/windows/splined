use std::error::Error;
use std::fmt;
use std::process::{Command, Stdio};
use std::time::Duration;
use wait_timeout::ChildExt;

const TICK_MAC_ADDR: &str = "88:51:fb:c8:e1:0d";
const SSH_TARGET: &str = "dashboard@tick.spline.de";
const TIMEOUT: Duration = Duration::from_secs(10);

#[derive(Debug)]
pub enum TickError {
    WakeOnLanError,
    SshError,
}

impl fmt::Display for TickError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use TickError::*;
        match self {
            WakeOnLanError => write!(
                f,
                "Failed to wake tick: wakeonlan returned failed status code"
            ),
            SshError => write!(
                f,
                "Failed to suspend tick: ssh process exited with an error code"
            ),
        }
    }
}

impl Error for TickError {}

pub fn report_open() -> Result<(), TickError> {
    let mut proc = Command::new("wakeonlan")
        .arg(TICK_MAC_ADDR)
        .stdin(Stdio::null())
        .stdout(Stdio::null())
        .spawn()
        .expect("Failed to wake tick: failed to spawn wakeonlan process");
    match proc.wait_timeout(TIMEOUT).unwrap() {
        Some(status) => {
            if !status.success() {
                return Err(TickError::WakeOnLanError);
            }
        }
        None => {
            // Timed out; kill and report error
            eprintln!("Failed to wake tick:wakeonlan process timed out");
            proc.kill().unwrap();
            return Err(TickError::WakeOnLanError);
        }
    }
    Ok(())
}

pub fn report_closed() -> Result<(), TickError> {
    let mut proc = Command::new("ssh")
        .arg(SSH_TARGET)
        .arg("sudo systemctl suspend")
        .stdin(Stdio::null())
        .stdout(Stdio::null())
        .spawn()
        .expect("Failed to suspend tick: failed to spawn ssh process");
    match proc.wait_timeout(TIMEOUT).unwrap() {
        Some(status) => {
            if !status.success() {
                return Err(TickError::SshError);
            }
        }
        None => {
            // Timed out; kill and report error
            eprintln!("Failed to suspend tick: SSH process timed out");
            proc.kill().unwrap();
            return Err(TickError::SshError);
        }
    }
    Ok(())
}
