use std::time::Duration;

const MATTERMOST_FQDN: &str = "chat.spline.de";
const DOOR_OPEN_MSG: &str = "The fucking door is open";
const DOOR_CLOSE_MSG: &str = "The door is closed";
const TIMEOUT: Duration = Duration::from_secs(10);

pub fn report_open(token: &str) -> Result<(), ureq::Error> {
    send(token, DOOR_OPEN_MSG)
}

pub fn report_closed(token: &str) -> Result<(), ureq::Error> {
    send(token, DOOR_CLOSE_MSG)
}

fn send(token: &str, text: &str) -> Result<(), ureq::Error> {
    let url = format!("https://{}/hooks/{}", MATTERMOST_FQDN, token);
    ureq::post(&url).timeout(TIMEOUT).send_json(ureq::json!({
        "text": text,
    }))?;

    Ok(())
}
