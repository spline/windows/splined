
export TARGET_CC := $(shell pwd)/toolchain/armv6-linux-musleabihf-cross/bin/armv6-linux-musleabihf-gcc
export PATH := $(shell pwd)/toolchain/armv6-linux-musleabihf-cross/bin/:$(PATH)


all:
	@echo 'Use `make release-nom` to build an executable suitable for running on the Raspberry Pi.'
	@echo 'Use `make deploy-nom` to deploy to nom.'

tmp/armv6-linux-musleabihf-cross.tgz:
	mkdir -p tmp
	wget https://musl.cc/armv6-linux-musleabihf-cross.tgz -O tmp/armv6-linux-musleabihf-cross.tgz

toolchain: tmp/armv6-linux-musleabihf-cross.tgz
	mkdir -p toolchain/
	tar -xf tmp/armv6-linux-musleabihf-cross.tgz -C toolchain

	rustup target add arm-unknown-linux-musleabihf

release-nom: toolchain
	cargo build --target arm-unknown-linux-musleabihf --release

deploy-nom: release-nom
	ssh root@nom.spline.de "systemctl stop splined-windowsd"
	scp target/arm-unknown-linux-musleabihf/release/splined root@nom.spline.de:/usr/local/bin/ && ssh root@nom.spline.de "systemctl restart splined-windowsd"

release-spaceapi-only: toolchain
	cargo build --target arm-unknown-linux-musleabihf --release --no-default-features
