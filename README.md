# Spline Goes Windows Reloaded

Splined is a daemon running on a Raspberry Pi which manages various things in the Spline Room, like updating SpaceAPI, posting door state changes to channels, updating the LED sign and turning on and off devices.

It can be used independently, just for its SpaceAPI update functionality, as done by the FSI Informatik.
See [their ansible role](https://gitlab.spline.de/fsi/ansible-playbook/-/blob/master/roles/door/tasks/main.yml?ref_type=heads) for details.

## Cross-compiling

This projects makefile contains targets to simplify cross-compiling and deployment.
You can run `make toolchain` to install a cross-toolchain in the project directory.
`make release` builds a release executable suitable for running on the Raspberry Pi.
`make run` can be used to deploy and run on nom.spline.de. This might need special ssh configuration
if used from outside the Spline network.
